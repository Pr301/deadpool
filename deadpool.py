from SixthSense import *
from time import sleep
from gpiozero import RGBLED
from Apollo import Apollo

press = DigitalPress(17)
hand = Hand(23, 24, 25, 26)
lLED = RGBLED(4, 6, 5)
apollo = Apollo()

hand.add_gesture({
    'name': 'Fuck you',
    'index': 'down',
    'middle': 'up',
    'ring': 'down',
    'little': 'down'
})
hand.add_gesture({
    'name': 'Victory',
    'index': 'up',
    'middle': 'up',
    'ring': 'down',
    'little': 'down'
})
hand.add_gesture({
    'name': 'Satan',
    'index': 'up',
    'middle': 'down',
    'ring': 'down',
    'little': 'up'
})
hand.add_gesture({
    'name': 'Sing',
    'index': 'up',
    'middle': 'down',
    'ring': 'up',
    'little': 'down'
})
hand.add_gesture({
    'name': 'Chorus',
    'index': 'down',
    'middle': 'up',
    'ring': 'down',
    'little': 'up'
})

ges_to_led = {
    'Fuck you': {'color': (1, 0, 0), 'blink': False},
    'Victory': {'color': (0, 0, 1), 'blink': False},
    'Satan': {'color': (1, 0, 0), 'blink': True},
    'Sing': {'color': (0, 1, 0), 'blink': True},
    'Chorus': {'color': (0, 0, 1), 'blink': True},
}

ges_to_music = {
    'Sing': '/home/pi/projects/deadpool/mp3/soundtrack.mp3',
    'Chorus': '/home/pi/projects/deadpool/mp3/chorus.mp3',
}

while True:
    ges = hand.gesture()

    if not apollo.playing():
        if ges:
            if ges_to_led[ges]['blink']:
                lLED.blink(0.05, 0.05, 0, 0, ges_to_led[ges]['color'])
            else:
                lLED.color = ges_to_led[ges]['color']
        else:
            lLED.color = (0, 0, 0)

    if press.is_active:
        if ges in ges_to_music:
            apollo.decide(ges_to_music[ges])
            sleep(1)
    sleep(0.1)
