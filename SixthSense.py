from gpiozero import Button


class DigitalFlex(Button):

    def __init__(self, pin):
        super(DigitalFlex, self).__init__(pin, False)

    def is_up(self):
        return self.is_pressed

    def is_down(self):
        return not self.is_pressed


class DigitalPress(Button):

    def __init__(self, pin):
        super(DigitalPress, self).__init__(pin, False)


class Hand():

    gestures = []

    def __init__(self, i, m, r, l):
        self.index = DigitalFlex(i)
        self.middle = DigitalFlex(m)
        self.ring = DigitalFlex(r)
        self.little = DigitalFlex(l)

    def gesture(self):
        # print(self.index.is_up())
        # print(self.middle.is_up())
        # print(self.ring.is_up())
        # print(self.little.is_up())

        for ges in self.gestures:
            if (
                getattr(self.index, 'is_%s' % ges['index'])() and
                getattr(self.middle, 'is_%s' % ges['middle'])() and
                getattr(self.ring, 'is_%s' % ges['ring'])() and
                getattr(self.little, 'is_%s' % ges['little'])()
            ):

                return ges['name']

    def add_gesture(self, ges):
        self.gestures.append(ges)
