from mutagen.mp3 import MP3
from subprocess import Popen
from time import time


class Apollo():

    current = ""
    end = 0

    def play(self, song):
        if not self.playing():
            self.proc = Popen(['mpg321', '-q', song])

            meta = MP3(song)
            self.end = time() + meta.info.length

    def stop(self):
        if self.playing():
            self.proc.terminate()
            self.end = 0

    def playing(self):
        return time() < self.end

    def decide(self, song):
        if self.playing():
            self.stop()
        else:
            self.play(song)
